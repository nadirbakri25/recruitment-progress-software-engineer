import java.util.Scanner;

public class Sorting {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Sorting with Bubble Sort");
        System.out.print("Array Length : ");
        int[] arr = new int[in.nextInt()];

        boolean isSorted = false;

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter a number : ");
            arr[i] = in.nextInt();
        }

        System.out.println();

        while (isSorted == false) {
            isSorted = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i+1]) {
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                    isSorted = false;
                }
            }
        }

        System.out.println("Sorting Result");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
