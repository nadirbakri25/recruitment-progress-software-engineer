import java.util.Scanner;

public class IntegerPalindrome {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Array Length :");
        int length = in.nextInt();

//        Get data
        int[] arr = new int[length];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter a number : ");
            arr[i] = in.nextInt();
        }

//        Reverse data
        int[] arrReversed = new int[arr.length];
        for (int i = 0; i < arrReversed.length; i++) {
            arrReversed[i] = reversed(arr[i]);
        }

//        Detect palindrome number
        System.out.println("The number of Palindrome : " + palindrome(arr, arrReversed));
    }

    public static int reversed(int number) {
        int result = 0;

        while (number != 0) {
            int getLastNumber = number % 10;
            result = result * 10 + getLastNumber;
            number /= 10;
        }

        return result;
    }

    public static int palindrome(int[] arr, int[] arrReversed) {
        int output = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == arrReversed[i])
                output++;
        }

        return output;
    }
}

